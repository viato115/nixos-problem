{
  config, 
  pkgs,
  user,
  inputs,
  hyprland,
  ... 
}: {

  imports = [
    /home/nico/.config/nixos/users/nico/pkgs/hypr/default.nix
    /home/nico/.config/nixos/users/nico/pkgs/kitty/default.nix
#    /home/nico/.config/nixos/users/nico/pkgs/greetd/default.nix

  ];

  # Packages to install for specified user
  home.packages = with pkgs; [

   # Basics
   greetd.tuigreet
   acpi
   kitty
   pfetch
   exa
   fortune 
   neovim
   ranger
   pcmanfm
   zathura
   mpv
   ffmpeg
   pavucontrol
   blueman
   ripgrep
   fzf
   trash-cli
   bat
   zip
   unzip
   hyprpaper
   feh
   firefox


   # Hyprland specific
   pulseaudio
   qt6.qtwayland
   libsForQt5.qt5.qtwayland

   (pkgs.nerdfonts.override { fonts = [ "Mononoki" ]; })
  ];


  services.gammastep = {
    enable = true;
    provider = "manual";
    latitude = 52.52;
    longitude = 13.40;
  };

 # colorScheme = nix-colors.colorSchemes.dracula;

#  programs = {
  
#    bash = {
#      enable = true;
#      interactiveShellInit = (builtins.readFile /home/nico/.bashrc);
#     # initExtra = ''
#     #   if test $(id --user $USER) = 1000 && test $(tty) = "/dev/tty1"
#     #   then
#     #     exec Hyprland
#     #   fi
#     # '';
#    };
    
#    kitty = {
#      enable = true;
#     # settings = {
#      #  foreground = "#{config.colorScheme.colors.base05}";
#      #  background = "#{config.colorScheme.colors.base00}";
#     # };
#    };
#  };

 # Manage dotfiles
 # home.file = {
 # };




  home.username = "nico";
  home.homeDirectory = "/home/nico";

  nixpkgs.config.allowUnfree = true;
  programs.home-manager.enable = true;  # Let Home Manager install and manage itself.
  home.stateVersion = "22.11";          # Please read the comment before changing.
}
