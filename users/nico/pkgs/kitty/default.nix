{
  pkgs,
  config,
  lib,
  ...
}: with lib; {

  programs.kitty = {
    enable = true;
    extraConfig = ''
      include color.ini

      font_family Mononoki Nerd Font Complete Regular
      bold_font auto
      italic_font auto
      bold_italic_font auto
      font_size 14.0
      disable_ligatures never
      font_features none
      
      cursor_stop_blinking_after 15.0

      scrollback_lines 2000
      scrollback_pager less --chop-long-lines --RAW-CONTROL-CHARS +INPUT_LINE_NUMBER
      scrollback_pager_history_size 0
      wheel_scroll_multiplier 5.0
      touch_scroll_multiplier 1.0
      mouse_hide_wait 3.0

      url_style curly
      open_url_with firefox
      open_url_modifiers kitty_mod

      sync_to_monitor yes
      input_delay 3
      enable_audio_bell no
      visual_bell_duration 0.0
      window_alert_on_bell no
      bell_on_tab no
      command_on_bell none

      remember_window_size no
      window_border_width 1.0
      draw_minimal_border yes
      placement_strategy center
      confirm_os_window_close 0

      background_opacity 1
      background_image none

      shell bash
      editor neovim
      close_on_child_death no
      listen_on none
      update_check_interval 24
      clipboard_control write-clipboard write-primary
      term xterm-kitty
      linux_display_server auto

      kitty_mod ctrl+shift
      map kitty_mod+c copy_to_clipboard
      map kitty_mod+v paste_from_clipboard

      cursor_shape Underline
      cursor_underline_thickness 1
      window_padding_width 10
      
      # Special
      foreground #a9b1d6
      background #1a1b26
      
      # Black
      color0 #414868
      color8 #414868
      
      # Red
      color1 #f7768e
      color9 #f7768e
      
      # Green
      color2  #73daca
      color10 #73daca
      
      # Yellow
      color3  #e0af68
      color11 #e0af68
      
      # Blue
      color4  #7aa2f7
      color12 #7aa2f7
      
      # Magenta
      color5  #bb9af7
      color13 #bb9af7
      
      # Cyan
      color6  #7dcfff
      color14 #7dcfff

      # White
      color7  #c0caf5
      color15 #c0caf5
      
      # Cursor
      cursor #c0caf5
      cursor_text_color #1a1b26
      
      # Selection highlight
      selection_foreground #7aa2f7
      selection_background #28344a
    '';
  };
}
