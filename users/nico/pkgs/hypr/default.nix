{
  pkgs,
  lib,
  ...
}: {
  home.packages = with pkgs; [ mako libnotify grim slurp pamixer];

  home.file.".config/hypr/hyprpaper.conf" = {
    text = ''
      preload = ~/.config/nixos/pics/wallpaper.png
      wallpaper = eDP-1, ~/.config/nixos/pics/wallpaper.png
    '';
  };


  home.file.".local/bin/wrappedhl" = {
    executable = true;
    text = ''
      #!/usr/bin/env bash
      
      cd ~
      
      export HYPRLAND_LOG_WLR=1
      
      export XCURSOR_SIZE=24
      
      export MOZ_ENABLE_WAYLAND=1
      export QT_QPA_PLATFORMTHEME=qt5ct
      
      exec Hyprland
    '';
  };

  wayland.windowManager.hyprland = {
    enable = true;
    xwayland = {
      enable = true;
      hidpi = true;
    };
    extraConfig = lib.fileContents ./hyprland.conf;
  };
}
